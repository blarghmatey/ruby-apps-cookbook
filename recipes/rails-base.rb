if node['ruby-apps']['downgrade_rubygems']
  execute "downgrade rubygems" do
    command "gem update --system 1.4.2"
    user "root"
  end
end

gem_package "bundler" do
  action :upgrade
end

gem_package "passenger" do
  action :install
  version "4.0.20"
end

include_recipe "passenger_apache2::default"
