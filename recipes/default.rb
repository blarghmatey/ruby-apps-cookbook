#
# Cookbook Name:: ruby-apps
# Recipe:: default
#
# Copyright (C) 2013 YOUR_NAME
#
# All rights reserved - Do Not Redistribute
#
%w{apt-utils vim libxml2-dev imagemagick libmagickwand-dev mysql-client-5.5 aspell ghostscript}.each do |pkg|
  apt_package pkg do
    action :install
  end
end

include_recipe "ruby_build::default"

ruby_build_ruby node['ruby-apps']['ruby_version']

template "/etc/profile.d/ruby-path.sh" do
  source "ruby-path.sh.erb"
  mode "0755"
end
