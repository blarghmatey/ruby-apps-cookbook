template "/etc/apache2/sites-available/#{node['ruby-apps']['app_name']}" do
  source "rails-site.erb"
  variables({
    :rails_env => node['ruby-apps']['rails_env'],
    :app_url => node['ruby-apps']['url_hash'][node['ruby-apps']['app_name']],
    :app_name => node['ruby-apps']['app_name']
  })
  mode 644
  group "root"
  owner "root"
end

link "/etc/apache2/sites-enabled/#{node['ruby-apps']['app_name']}" do
  to "/etc/apache2/sites-available/#{node['ruby-apps']['app_name']}"
end

user "rails" do
  home "/home/rails"
end

directory "/home/rails/#{node['ruby-apps']['app_name']}/current" do
  recursive true
  owner "rails"
end
