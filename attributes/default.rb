default['ruby-apps']['ruby_version'] = "1.8.7-p374"
default['ruby-apps']['rails_env'] = "development"
default['ruby-apps']['tld_hash'] = {
  :development => "dev",
  :backstage => "com",
  :staging => "com",
  :production => "com"
}
default['ruby-apps']['url_hash'] = {
  :mc => "mc.secondrotation.#{node['ruby-apps']['tld_hash'][node['ruby-apps']['rails_env']]}",
  :g3 => "gazelle.#{node['ruby-apps']['tld_hash'][node['ruby-apps']['rails_env']]}",
  :ic => "ic.secondrotation.#{node['ruby-apps']['tld_hash'][node['ruby-apps']['rails_env']]}",
  :srapi => "api.gazelle.#{node['ruby-apps']['tld_hash'][node['ruby-apps']['rails_env']]}"
}
default['ruby-apps']['app_name'] = "g3"
default['ruby-apps']['downgrade_rubygems'] = true
